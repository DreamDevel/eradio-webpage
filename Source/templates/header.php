<div id="header" class="container">
  <div class="row">
    <div class="col-md-7 col-sm-5 col-xs-0 hidden-xs">
      <img class="logo" src=<?= $link->image("logo.png") ?> onclick='location.href=<?= $link->page("home") ?>'/>
      <div class="logo-subtitle" onclick='location.href=<?= $link->page("home") ?>'>A minimalist and powerful radio player</div>
    </div>
    <div class="col-md-5 col-sm-7 col-xs-12 col-xs-no-padding">
      <nav>
        <div class="mobile-nav-button hidden-sm hidden-md hidden-lg" action="open" onclick="navButtonPressed(this)"/></div>
        <img class="mobile-logo hidden-sm hidden-md hidden-lg" src=<?= $link->image("mobile-logo.png") ?>/>
        <a href=<?= $link->page("home")?> class="first hidden-xs <?= $PAGE == "home" ? "current" : "" ?>">HOME/DOWNLOAD</a>
        <a href=<?= $link->page("packages")?> class="hidden-xs <?= $PAGE == "packages" ? "current" : "" ?>">GET STATIONS</a>
        <a href=<?= $link->page("donate")?> class="last hidden-xs <?= $PAGE == "donate" ? "current" : "" ?>">DONATE</a>
      </nav>
    </div>
  </div>
</div>

<div id="mobile-nav" class="hidden-sm hidden-md hidden-lg">
  <ul>
    <li><a href=<?= $link->page("home")?> class="<?= $PAGE == "home" ? "current" : "" ?>">Home</a></li>
    <li><a href=<?= $link->page("donate")?> class="<?= $PAGE == "donate" ? "current" : "" ?>">Donate</a></li>
  </ul>
</div>