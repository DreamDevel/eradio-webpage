<script>
var packages = [];

String.prototype.replaceAll = function( token, newToken, ignoreCase ) {
    var _token;
    var str = this + "";
    var i = -1;

    if ( typeof token === "string" ) {

        if ( ignoreCase ) {

            _token = token.toLowerCase();

            while( (
                i = str.toLowerCase().indexOf(
                    _token, i >= 0 ? i + newToken.length : 0
                ) ) !== -1
            ) {
                str = str.substring( 0, i ) +
                    newToken +
                    str.substring( i + token.length );
            }

        } else {
            return this.split( token ).join( newToken );
        }

    }
return str;
};

String.prototype.contains = function(it) { return this.indexOf(it) != -1; };

function navButtonPressed(element) {

    var isCloseAction = $(element).attr("action") == "close";
    if (isCloseAction) {
        $("#mobile-nav").css("height","0%");
        $(element).attr("action","open")
        $("body").css("overflow","auto");
    } else {
        $("#mobile-nav").css("height","100%");
        $("body").css("overflow","hidden");
        $(element).attr("action","close")
    }
}

$(document).ready(function () {
    var BreakException = {};
    if (window.location.pathname.replaceAll("/","") == "packages") {
        update_packages();

        $(".country-input").keyup(function(){

            var input_text = $(this).val();
            try {
                packages.forEach(function(package){
                    if(package.country.toLowerCase() == input_text.toLowerCase()) {

                        $(".country").text(package.country);
                        $(".package-download-link").attr("href",package.url);
                        $(".country-package-not-found").hide();
                        $(".country-package-found").show();
                        throw BreakException;
                    } else if (input_text == "") {
                        $(".country-package-not-found").hide();
                        $(".country-package-found").hide();
                    } else {
                        $(".input-entry").text(input_text);
                        $(".country-package-not-found").show();
                        $(".country-package-found").hide();
                    } 
                });
            } catch (e) {

            }
        });

    }
}); 



function update_packages() {
    var main_url = "https://api.github.com";
    var packages_dir = "/repos/DreamDevel/eRadio-Packages/contents/Packages";

    $.ajax({
          url: main_url + packages_dir,
          success: function (data, textStatus, jqXHR) {
            data.forEach(function(file_obj,index) {
                var package = {
                    "country" : file_obj.name.replace(".erpkg",""),
                    "url" : file_obj.download_url
                };

                packages.push(package);
            });
          }
        });
}
</script>