<?php include "system/helper.php" ?>
<html>
  <head>
    <!-- Revision: <?= $BUILD_NUMBER  ?> -->
    <?php include "templates/head.php"; ?>
  </head>
  <body>
    <?php include "templates/header.php"; ?>
    <?php include $PAGE_PATH; ?>
    <?php include "templates/scripts.php"; ?>
    <?php include "templates/footer.php"; ?>
  </body>
</html>
