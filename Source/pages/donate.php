<div id=donate-page class="container text-center">
<div class="row">

</div>
<div class="row">
	<div class="col-lg-12">
		<h1>Support Dream Development</h1>
	</div>
</div>

<div class="row">
	<div class="col-lg-12">
		<p class="hidden-xs">
		Donations gives us the opportunity to work more on our <br>
		apps instead of working on freelance projects to make a <br>
		living. If you find our apps helpful consider donating.
		</p>

		<p class="hidden-sm hidden-md hidden-lg">
		Donations gives us the opportunity <br>
		to work more on our apps instead <br>
		of working on freelance projects to <br>
		make a living. If you find our apps <br>
		helpful consider donating.
		</p>
	</div>
</div>

<div class="row">
	<div class="col-lg-12">
		<input type=button class="donate-button" value="Paypal | Donate" onclick='location.href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=2TKHC6ZW2JLSJ"'>
	</div>
</div>

</div>