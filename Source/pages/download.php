<div id=download-page class="container text-center">

  <div class="row install-box hidden-xs">
    <div class="col-md-12">
      <h1>Installation</h1>
      <div class="method"><strong>Open terminal and run</strong><br>
      <span>wget http://eradio.dreamdevel.com/download/install-eradio.sh && sh install-eradio.sh<span></div>
    </div>
  </div>

  <div class="row hidden-xs">
    <div class="col-md-12">
      <h1>What's Next?</h1>
    </div>
  </div>

  <div class="row suggestions-box hidden-xs">

    <div class="col-md-6 col-lg-4 text-center">
      <div class="suggestion">
        <h2>Download Country Package</h2>
        <p><a href=<?= $link->page("packages") ?>>Download Country Package</a> to import your<br> 
           country's radio stations.</p>
      </div>
    </div>
    <div class="col-md-6 col-lg-4 text-center">
      <div class="suggestion">
        <h2>Download Web Extension</h2>
        <p>Add easily radio station through your web <br> 
           browser with <a href=<?= $link->page("packages") ?>>eRadio Web Extension</a></p>
      </div>
    </div>
    <div class="col-md-12 col-lg-4 text-center">
      <div class="suggestion">
        <h2>Support Development</h2>
        <p>If you find eRadio helpful, support <br>
          Dream Development by <a href=<?= $link->page("donate") ?>>Donating</a></p>
      </div>
    </div>

  </div>


  <div class="row hidden-sm hidden-md hidden-lg">
    <h2>Download is not available <br>
    for mobile devices</h2>
  </div>

  <div class="row more-details hidden-sm hidden-md hidden-lg">
    <i>For more details and downloading
    please visit this page through a <br>
    desktop web browser.</i>
  </div>

</div>