<div id="home-page" class="container">
  <div class="row">
    <div class="col-lg-8 col-md-7 col-xs-12">
      <img class="screenshot" src=<?= $link->image("home/screenshot.png") ?> />
    </div>
    <div class="col-lg-4 col-md-5 col-xs-12 text-left download-box">
      
      <div class="wrapper">
      <h1>Download for free</h1>
      <div class="subheader">Stream your favorite radio stations from <br class="hidden-xs">
           your desktop with integraded OS features.</div>
      <div class="hr hidden-xs"></div>
      <div class="ul-container hidden-xs">
        <ul>
            <li>Support for popular stream types</li>
            <li>Add stations <span class="hidden-xs">easily</span> through <span class="hidden-xs">your</span> web browser</li>
            <li>Import & backup stations <span class="hidden-xs">with eRadio packages</span></li>
            <li>Sound menu integration</li>
            <li>Media keys support</li>
        </ul>
      </div>
      <div class="hr-bottom hidden-xs"></div>
      <input type=button class="download-button hidden-xs" value="Download" onclick='location.href=<?= $link->page("download") ?>'>
      <a href ="http://github.com/DreamDevel/eRadio" class="github-button hidden-xs"><img alt="Source Code" src=<?= $link->image("home/github.png") ?>
          onmouseover='$(this).attr("src",<?= $link->image("home/github-hover.png") ?>)'
          onmouseout='$(this).attr("src",<?= $link->image("home/github.png") ?>)' /></a>
        <div class="target-os  hidden-xs">
          <strong>Supported OS:</strong> <span> elementary OS Loki </span>
        </div>
      </div>
    </div>
  </div>

  <div class="row hidden-sm hidden-md hidden-lg text-center feature" index="1">
    <div class="col-xs-12">
      <h2>Stream Any Radio Station</h2>
      <i class="fa fa-play-circle fa-4x" aria-hidden="true"></i>
      <p>eRadio supports many stream types<br>
         including asx, m3u & pls.</p>
    </div>
  </div>

  <div class="row hidden-sm hidden-md hidden-lg text-center feature" index="2">
    <div class="col-xs-12">
      <h2>Web Browser Integration</h2>
      <i class="fa fa-link fa-4x" aria-hidden="true"></i>
      <p>eRadio support webradio URL Handler. <br>
         You can add stations with a single click.</p>
    </div>
  </div>

  <div class="row hidden-sm hidden-md hidden-lg text-center feature" index="1">
    <div class="col-xs-12">
      <h2>Chrome & Firefox Web Extension</h2>
      <i class="fa fa-puzzle-piece fa-4x" aria-hidden="true"></i>
      <p>You can use eRadio web extension <br>
        to easily add stations through TuneIn</p>
    </div>
  </div>


  <div class="row hidden-sm hidden-md hidden-lg text-center feature" index="2">
    <div class="col-xs-12">
      <h2>Import & Backup Stations</h2>
      <i class="fa fa-archive fa-4x" aria-hidden="true"></i>
      <p>Create packages throught eRadio <br>
         for backing up & sharing</p>
    </div>
  </div>

  <div class="row hidden-sm hidden-md hidden-lg text-center feature" index="1">
    <div class="col-xs-12">
      <h2>Sound Menu Integration<br>
          Media Keys Support</h2>
      <i class="fa fa-keyboard-o fa-4x" aria-hidden="true"></i>
      <p>Change stations via your keyboard <br>
          or Sound indcator easily & fast</p>
    </div>
  </div>

  <div class="row hidden-sm hidden-md hidden-lg text-center feature" index="2">
    <div class="col-xs-12">
      <h2>Free & Open Source</h2>
      <i class="fa fa-code-fork fa-4x" aria-hidden="true"></i>
      <p>eRadio is free of charge and <br>
         open source, start hacking.</p>
    </div>
  </div>

  <div class="row hidden-sm hidden-md hidden-lg text-center feature" index="x">
    <div class="col-xs-12">
      <h2>Download</h2>
      <i class="fa fa-download fa-4x" aria-hidden="true"></i>
      <p>Visit this page from <br>
      your desktop to download!</p>
    </div>
  </div>
</div>
