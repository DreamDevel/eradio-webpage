<div id="packages-page" class="container">

	<div class="row  hidden-xs">
		<div class="col-lg-12 text-center">
			<h1>Get Radio Stations</h1>
		</div>
	</div>
	
	<div class="row  hidden-xs">
		<div class="col-lg-6">
			<div class="country-solution-wrapper">
				<h2>Country Packages</h2>
				<p>
				Download your country's or state's eRadio package and import it.<br>
				These packages are contributions from people all over the world.
				</p>
				<div class="input-wrapper">
					<input class="country-input" type="text" placeholder="Enter your country or US state" />
					<img src=<?= $link->image("search.png") ?> />
				</div>
				<p class="country-package-found">Package "<span class="country">Greece</span>" found. <a class="package-download-link" download>Download Package</a></p>
				<p class="country-package-not-found">Package "<span class="input-entry"></span>" doesn't exists. <br> Would you like to <a href="https://github.com/DreamDevel/eRadio-Packages">contribute?</a></p>
			</div>
		</div>

		<div class="col-lg-6">
			<div class="extension-solution-wrapper">
				<h2>Web Extension</h2>
				<p>
				eRadio uses webradio URI handler to import stations from the web.<br>
				We created an extension for TuneIn that adds a button to add station<br>
				to eRadio. Download the <a href ="https://chrome.google.com/webstore/detail/eradio-web-extension/hfcmlafjdledobfkeadflgldbhfbljan">Chrome Extension</a> or <a href="https://addons.mozilla.org/en-US/firefox/addon/eradio-web-extension/">Firefox Extension</a>.
				</p>
			</div>
		</div>
	</div>

	<div class="row  hidden-xs">
		<div class="col-lg-6 col-lg-offset-6">
			<div class="discover-solution-wrapper">
				<h2>Discover (WIP)</h2>
				<p>
				You may have noticed the Discover category in eRadio v2.0. This is a <br>
				placeholder for the next version where you will be able to easily find <br>
				stations within the app and save them to your library.
				</p>
			</div>
		</div>
	</div>


	<div class="row hidden-sm hidden-md hidden-lg text-center">
	  <h2>This page is not available <br>
	  for mobile devices</h2>
	</div>

	<div class="row more-details hidden-sm hidden-md hidden-lg">
	  <i>For more details and downloading
	     please visit this page through a <br>
	     desktop web browser.</i>
	</div>
</div>