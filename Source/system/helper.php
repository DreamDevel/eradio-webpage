<?php
$PAGE_PATH = $argv[1];
$BUILD_NUMBER = $argv[2];
$PAGE = str_replace(".php","",str_replace("pages/","",$PAGE_PATH));
$PAGES_PATH_PREFIX = "../";

class Link {
  public function base($path) {
    global $PAGE, $PAGES_PATH_PREFIX;

    if ($PAGE == "home")
      return '"' . $path . '"';
    else
      return '"' . $PAGES_PATH_PREFIX . $path  . '"';
  }

  public function image($imageName) {
    global $PAGE, $PAGES_PATH_PREFIX;

    $path = "images/" . $imageName;
    if ($PAGE == "home")
      return '"' . $path . '"';
    else
      return '"' . $PAGES_PATH_PREFIX . $path  . '"';
  }

  public function css($cssName) {
    global $PAGE, $PAGES_PATH_PREFIX;

    $path = "css/" . $cssName;
    if ($PAGE == "home")
      return '"' . $path . '"';
    else
      return '"' . $PAGES_PATH_PREFIX . $path  . '"';
  }

  public function page($pageName) {

    if ($pageName == "home")
      return '"' . "/" . '"';
    else
      return '"' . "/" . $pageName . "/"  . '"';
  }
}

$link = new Link();

?>
