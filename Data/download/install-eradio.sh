echo " -----------------------"
echo "| eRadio Install Script |"
echo " -----------------------"
echo "Dream Development PPA & eRadio will be installed."
echo "Press any key to continue..."
read a
sudo apt-get -y install software-properties-common
sudo add-apt-repository -y ppa:dreamdevel/stable
sudo apt-get update
sudo apt-get install eradio